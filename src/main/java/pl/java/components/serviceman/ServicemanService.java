package pl.java.components.serviceman;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java.components.reapair.RepairDto;
import pl.java.components.reapair.RepairMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ServicemanService {

    private ServicemanRepository servicemanRepository;
    private ServicemanMapper servicemanMapper;
    private RepairMapper repairMapper;

    @Autowired
    public ServicemanService(ServicemanRepository servicemanRepository, ServicemanMapper servicemanMapper, RepairMapper repairMapper) {
        this.servicemanRepository = servicemanRepository;
        this.servicemanMapper = servicemanMapper;
        this.repairMapper = repairMapper;
    }


    List<ServicemanDto> findAll() {
        return servicemanRepository.findAll()
                .stream()
                .map(servicemanMapper::toDto)
                .collect(Collectors.toList());
    }

    List<ServicemanDto> findAllByInitials(String text) {
        return servicemanRepository.findAllByInitials(text)
                .stream()
                .map(servicemanMapper::toDto)
                .collect(Collectors.toList());
    }
    List<String> findAllInitials() {
        return servicemanRepository.findAll()
                .stream()
                .map(Serviceman::getInitials)
                .collect(Collectors.toList());
    }

    List<RepairDto> getServicemanRepair(Long servicemanId){
        return servicemanRepository.findById(servicemanId)
                .map(Serviceman::getRepairs)
                .orElseThrow(ServicemanNotFoundException::new)
                .stream()
                .map(repairMapper::toDto)
                .collect(Collectors.toList());
    }

    Optional<ServicemanDto> findById(Long id) {
        return servicemanRepository.findById(id).map(servicemanMapper::toDto);
    }

    public ServicemanDto save(ServicemanDto serviceman) {
        return mapAndSaveUser(serviceman);
    }

    public ServicemanDto update(ServicemanDto serviceman) {
        return mapAndSaveUser(serviceman);
    }

    private ServicemanDto mapAndSaveUser(ServicemanDto serviceman) {
        Serviceman servicemanEntity = servicemanMapper.toEntity(serviceman);
        Serviceman saveServiceman = servicemanRepository.save(servicemanEntity);
        return servicemanMapper.toDto(saveServiceman);
    }
}
