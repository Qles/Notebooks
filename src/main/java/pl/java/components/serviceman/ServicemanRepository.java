package pl.java.components.serviceman;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ServicemanRepository extends JpaRepository<Serviceman, Long> {
    Optional<Serviceman> findAllByInitials(String search);
}
