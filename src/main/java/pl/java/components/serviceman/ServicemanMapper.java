package pl.java.components.serviceman;

import org.springframework.stereotype.Service;

@Service
public class ServicemanMapper {

    public ServicemanDto toDto(Serviceman serviceman) {
        ServicemanDto dto = new ServicemanDto();
        dto.setId(serviceman.getId());
        dto.setFirstName(serviceman.getFirstName());
        dto.setLastName(serviceman.getLastName());
        dto.setInitials(serviceman.getInitials());
        return dto;
    }

    public Serviceman toEntity(ServicemanDto servicemanDto) {
        Serviceman entity = new Serviceman();
        entity.setId(servicemanDto.getId());
        entity.setFirstName(servicemanDto.getFirstName());
        entity.setLastName(servicemanDto.getLastName());
        entity.setInitials(servicemanDto.getInitials());
        return entity;
    }
}
