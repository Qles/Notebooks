package pl.java.components.serviceman;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Brak serwisanta o takim ID")
public class ServicemanNotFoundException extends RuntimeException {
}
