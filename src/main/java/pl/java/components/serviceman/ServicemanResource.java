package pl.java.components.serviceman;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.java.components.reapair.RepairDto;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/servicemans")
public class ServicemanResource {

    private ServicemanService servicemanService;

    public ServicemanResource(ServicemanService serviceManService) {
        this.servicemanService = serviceManService;
    }

    @GetMapping("")
    public List<ServicemanDto> findAllByInitials(@RequestParam(required = false) String initials) {
        if (initials != null)
            return servicemanService.findAllByInitials(initials);
        else
            return servicemanService.findAll();
    }

    @GetMapping("/{id}")
        public ResponseEntity<ServicemanDto> findById(@PathVariable Long id) {
            return servicemanService.findById(id)
                    .map(ResponseEntity::ok)
                    .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/initials")
    public List<String> findAllInitials(){
        return servicemanService.findAllInitials();
    }

    @GetMapping("/{id}/repairs")
    public List<RepairDto> getServicemanRepairs(@PathVariable Long id) {
        return servicemanService.getServicemanRepair(id);
    }

    @PostMapping("")
    public ResponseEntity<ServicemanDto> save(@RequestBody ServicemanDto serviceman) {
        if(serviceman.getId() !=null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Zapisywany obiekt nie może mieć ustawionego ID");
        ServicemanDto saveServiceman = servicemanService.save(serviceman);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(saveServiceman.getId())
                .toUri();
        return ResponseEntity.created(location).body(saveServiceman);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ServicemanDto> update(@PathVariable Long id, @RequestBody ServicemanDto serviceman) {
        if(!id.equals(serviceman.getId()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Aktualizowany obiekt musi mieć id zgodnie z id w ściężce zasobu");
        ServicemanDto updateServiceman = servicemanService.update(serviceman);
        return ResponseEntity.ok(updateServiceman);
    }

}
