package pl.java.components.reapair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("api/repairs")
public class RepairResource {

    private RepairService repairService;

    @Autowired
    public RepairResource(RepairService repairService) {
        this.repairService = repairService;
    }

    @GetMapping("")
    public List<RepairDto> findAll(@RequestParam(required = false) String text) {
        if (text != null)
            return repairService.findByAll(text);
        else
            return repairService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<RepairDto> findById(@PathVariable Long id) {
        return repairService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }


    @PostMapping("")
    public ResponseEntity<RepairDto> save(@RequestBody RepairDto repair) {
        if (repair.getId() != null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Zapisywany obiekt nie może mieć ustawionego ID");
        RepairDto saveRepair = repairService.save(repair);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(saveRepair.getId())
                .toUri();
        return ResponseEntity.created(location).body(saveRepair);
    }

    @PutMapping("/{id}")
    public ResponseEntity<RepairDto> update(@PathVariable Long id, @RequestBody RepairDto repair) {
        if (!id.equals(repair.getId()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Zapisywany obiket ma ID różne od id w ścieżce zasobu");
        RepairDto updateRepair = repairService.update(repair);
        return ResponseEntity.ok(updateRepair);
    }


}
