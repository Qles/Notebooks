package pl.java.components.reapair;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RepairRepository extends JpaRepository<Repair, Long> {
    @Query("select r from Repair r where lower(r.defect) like lower(concat('%', :search, '%')) " +
            "or lower(r.repairDescription) like lower(concat('%', :search, '%')) " +
            "or lower(r.client) like lower(concat('%', :search, '%'))")
           // "or lower(r.equipment) like lower(concat('%', :search, '%')) " +
           // "or lower(r.equipment) like lower(concat('%', :search, '%')) " +
            //"or lower(r.serviceMan) like lower(concat('%', :search, '%'))")
    List<Repair> findByAll(@Param("search") String search);

    List<Repair> findByServiceman(String serviceman);

    }

