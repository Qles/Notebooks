package pl.java.components.reapair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RepairService {

    private RepairRepository repairRepository;
    private RepairMapper repairMapper;

    @Autowired
    public RepairService(RepairRepository repairRepository, RepairMapper repairMapper) {
        this.repairRepository = repairRepository;
        this.repairMapper = repairMapper;
    }

    public Optional<RepairDto> findById (Long id) {
        return repairRepository.findById(id).map(repairMapper::toDto);
    }

    public List<RepairDto> findByAll(String text) {
        return repairRepository.findByAll(text)
                .stream()
                .map(repairMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<RepairDto> findByServicemanInitials(String text) {
        return repairRepository.findByServiceman(text)
                .stream()
                .map(repairMapper::toDto)
                .collect(Collectors.toList());
    }

    List<RepairDto> findAll() {
        return repairRepository.findAll()
                .stream()
                .map(repairMapper::toDto)
                .collect(Collectors.toList());
    }

    public RepairDto save(RepairDto repair) {
        return mapAndSaveRepair(repair);
    }

    public RepairDto update(RepairDto repair) {
        return  mapAndSaveRepair(repair);
    }

    private RepairDto mapAndSaveRepair(RepairDto repairDto) {
        Repair repairEntity = repairMapper.toEntity(repairDto);
        Repair saveRepair = repairRepository.save(repairEntity);
        return repairMapper.toDto(saveRepair);
    }
}
