package pl.java.components.reapair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java.components.client.Client;
import pl.java.components.client.ClientRepository;
import pl.java.components.equipment.Equipment;
import pl.java.components.equipment.EquipmentRepository;
import pl.java.components.serviceman.Serviceman;
import pl.java.components.serviceman.ServicemanRepository;
import pl.java.components.status.Status;
import pl.java.components.status.StatusRepository;

import java.util.Optional;

@Service
public class RepairMapper {

    private ServicemanRepository serviceManRepository;
    private ClientRepository clientRepository;
    private EquipmentRepository equipmentRepository;
    private StatusRepository statusRepository;

    @Autowired
    public RepairMapper(ServicemanRepository serviceManRepository, ClientRepository clientRepository, EquipmentRepository equipmentRepository, StatusRepository statusRepository) {
        this.serviceManRepository = serviceManRepository;
        this.clientRepository = clientRepository;
        this.equipmentRepository = equipmentRepository;
        this.statusRepository = statusRepository;
    }

    public RepairDto toDto(Repair repair) {
        RepairDto dto = new RepairDto();
        dto.setId(repair.getId());
        dto.setDefect(repair.getDefect());
        dto.setRepairDescription(repair.getRepairDescription());
        dto.setStart(repair.getStart());
        dto.setEnd(repair.getEnd());
        dto.setWarranty(repair.getWarranty());
        dto.setPrice(repair.getPrice());
        dto.setCost(repair.getCost());
        if(repair.getStatus() !=null)
            dto.setStatus(repair.getStatus().getName());
        //Serviceman serviceMan = repair.getServiceman();
       // dto.setServiceManInitials(serviceMan.getInitials());
        //Client client = repair.getClient();
        //dto.setClientName(client.getName());
        if(repair.getClient() !=null)
            dto.setClientName(repair.getClient().getName());
       //Equipment equipment = repair.getEquipment();
        //dto.setEquipmentName(equipment.getName());
        if(repair.getEquipment() !=null)
            dto.setEquipmentName(repair.getEquipment().getName());
            dto.setEquipmentSerialNumber(repair.getEquipment().getSerialNumber());
        if(repair.getServiceman() !=null)
            dto.setServiceManInitials(repair.getServiceman().getInitials());
        return dto;
    }

    public Repair toEntity(RepairDto repair) {
        Repair entity = new Repair();
        entity.setId(repair.getId());
        entity.setDefect(repair.getDefect());
        entity.setRepairDescription(repair.getRepairDescription());
        entity.setStart(repair.getStart());
        entity.setEnd(repair.getEnd());
        entity.setWarranty(repair.getWarranty());
        entity.setPrice(repair.getPrice());
        entity.setCost(repair.getCost());
        Optional<Status> status = statusRepository.findByName(repair.getStatus());
        status.ifPresent(entity::setStatus);
        Optional<Serviceman> serviceMan = serviceManRepository.findAllByInitials(repair.getServiceManInitials());
        serviceMan.ifPresent(entity::setServiceman);
        Optional<Client> client = clientRepository.findByName(repair.getClientName());
        client.ifPresent(entity::setClient);
        Optional<Equipment> equipment = equipmentRepository.findByName(repair.getEquipmentName());
        equipment.ifPresent(entity::setEquipment);

        return entity;
    }
}



