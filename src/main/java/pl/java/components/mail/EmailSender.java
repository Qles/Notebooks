package pl.java.components.mail;

public interface EmailSender {
    void sendEmail(EmailDto emailDto);
}
