package pl.java.components.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/statuses")
public class StatusResource {


    private StatusService statusService;

    @Autowired
    public StatusResource(StatusService statusService) {
        this.statusService = statusService;

    }

    @GetMapping("/names")
    public List<String> findAllNames() {
        return statusService.findAllNames();
    }
}
