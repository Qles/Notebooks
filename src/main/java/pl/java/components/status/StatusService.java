package pl.java.components.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StatusService {

    private StatusRepository statusRepository;

    @Autowired
    public StatusService(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

        public List<String> findAllNames(){
        return statusRepository.findAll()
                .stream()
                .map(Status::getName)
                .collect(Collectors.toList());
    }
}
