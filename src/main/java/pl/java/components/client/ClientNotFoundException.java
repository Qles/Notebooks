package pl.java.components.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Brak klienta o takim ID")
public class ClientNotFoundException extends RuntimeException {
}
