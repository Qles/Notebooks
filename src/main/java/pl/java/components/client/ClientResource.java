package pl.java.components.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.java.components.reapair.RepairDto;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("api/clients")
public class ClientResource {

    private ClientService clientService;

    @Autowired
    public ClientResource(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("")
    public List<ClientDto> findByNameOrTelephoneNumber(@RequestParam(required = false) String text) {
        if(text != null)
            return clientService.findByNameOrTelephoneNumber(text);
            else
        return clientService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientDto> findById(@PathVariable Long id) {
        return clientService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/names")
    public List<String> findAllNames () {
        return clientService.findAllNames();
    }

    @GetMapping("/{id}/repairs")
    public List<RepairDto> getClientRepairs(@PathVariable Long id) {
        return clientService.getClientRepair(id);

    }

    @PostMapping("")
    public ResponseEntity<ClientDto> save(@RequestBody ClientDto client) {
        if (client.getId() != null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Zapisywany obiekt nie może mięc ustawionego id");
        ClientDto saveClient = clientService.save(client);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(saveClient.getId())
                .toUri();
        return ResponseEntity.created(location).body(saveClient);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ClientDto> update(@PathVariable Long id, @RequestBody ClientDto client) {
        if (!id.equals(client.getId()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Aktualizowany obiekt musi mieć id zgodnie z id w ściężce zasobu");
        ClientDto updatedUser = clientService.update(client);
        return ResponseEntity.ok(updatedUser);
    }
}

