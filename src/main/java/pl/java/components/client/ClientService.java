package pl.java.components.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java.components.reapair.RepairDto;
import pl.java.components.reapair.RepairMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientService {

    private ClientRepository clientRepository;
    private ClientMapper clientMapper;
    private RepairMapper repairMapper;

    public ClientService(ClientRepository clientRepository, ClientMapper clientMapper, RepairMapper repairMapper) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
        this.repairMapper = repairMapper;
    }

    Optional<ClientDto> findById(Long id) {
        return clientRepository.findById(id).map(clientMapper::toDto);
    }


    List<ClientDto> findAll() {
        return clientRepository.findAll()
                .stream()
                .map(clientMapper::toDto)
                .collect(Collectors.toList());
    }

    List<ClientDto> findByNameOrTelephoneNumber(String text) {
        return clientRepository.findByNameOrTelephoneNumber(text)
                .stream()
                .map(clientMapper::toDto)
                .collect(Collectors.toList());
    }
    List<String> findAllNames () {
        return clientRepository.findAll()
                .stream()
                .map(Client::getName)
                .collect(Collectors.toList());


    }
    List<RepairDto> getClientRepair(Long clientId) {
        return clientRepository.findById(clientId)
                .map(Client::getRepairs)
                .orElseThrow(ClientNotFoundException::new)
                .stream()
                .map(repairMapper::toDto)
                .collect(Collectors.toList());
    }

    ClientDto save(ClientDto client) {
        return mapAndSaveUser(client);
    }

    ClientDto update(ClientDto client) {
        return mapAndSaveUser(client);
    }

    private ClientDto mapAndSaveUser(ClientDto client) {
        Client clientEntity = clientMapper.toEntity(client);
        Client saveClient = clientRepository.save(clientEntity);
        return clientMapper.toDto(saveClient);
    }
}
