package pl.java.components.client;

import org.springframework.stereotype.Service;

@Service
public class ClientMapper {

    public ClientDto toDto (Client client) {
        ClientDto dto = new ClientDto();
        dto.setId(client.getId());
        dto.setName(client.getName());
        dto.setAddress(client.getAddress());
        dto.setPostalCode(client.getPostalCode());
        dto.setCity(client.getCity());
        dto.setTelephoneNumber(client.getTelephoneNumber());
        dto.setNip(client.getNip());

        return dto;
    }
    public Client toEntity(ClientDto client) {
        Client entity = new Client();
        entity.setId(client.getId());
        entity.setName(client.getName());
        entity.setAddress(client.getAddress());
        entity.setPostalCode(client.getPostalCode());
        entity.setCity(client.getCity());
        entity.setTelephoneNumber(client.getTelephoneNumber());
        entity.setNip(client.getNip());
        return entity;
    }
}
