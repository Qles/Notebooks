package pl.java.components.equipment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.java.components.client.ClientDto;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/equipments")
public class EquipmentResource {

    EquipmentService equipmentService;

    @Autowired
    public EquipmentResource(EquipmentService equipmentService) {
        this.equipmentService = equipmentService;
    }

    @GetMapping("")
    public List<EquipmentDto> findAllByNameOrSerialNumber(@RequestParam(required = false) String text) {
        if (text != null)
            return equipmentService.findAllByNameOrSerialNumber(text);
        else
            return equipmentService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<EquipmentDto> findById(@PathVariable Long id) {
        return equipmentService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/names")
    public List<String> findAllNames () {
        return equipmentService.findAllNames();
    }


    @PostMapping("")
    public ResponseEntity<EquipmentDto> save(@RequestBody EquipmentDto equipment) {
        if (equipment.getId() != null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Zapisywany Obiekt nie może mieć ustawionego Id");
        EquipmentDto saveEquipment = equipmentService.save(equipment);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(saveEquipment.getId())
                .toUri();
        return ResponseEntity.created(location).body(saveEquipment);

    }

    @PutMapping("/{id}")
    public ResponseEntity<EquipmentDto> update(@PathVariable Long id, @RequestBody EquipmentDto equipment) {
        if(!id.equals(equipment.getId()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Aktuzalizowany Obiekt musi mieć zgodnie ID z ID w ścieżce zasobu");
        EquipmentDto updateEquipment = equipmentService.update(equipment);
        return ResponseEntity.ok(updateEquipment);


    }
}

