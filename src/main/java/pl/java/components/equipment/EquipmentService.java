package pl.java.components.equipment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class EquipmentService {

    EquipmentRepository equipmentRepository;
    EquipmentMapper equipmentMapper;

    @Autowired
    public EquipmentService(EquipmentRepository equipmentRepository, EquipmentMapper equipmentMapper) {
        this.equipmentRepository = equipmentRepository;
        this.equipmentMapper = equipmentMapper;
    }

    public Optional<EquipmentDto> findById(Long id) {
        return equipmentRepository.findById(id).map(equipmentMapper::toDto);
    }


    public List<EquipmentDto> findAll() {
        return equipmentRepository.findAll()
                .stream()
                .map(equipmentMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<EquipmentDto> findAllByNameOrSerialNumber(String text) {
        return equipmentRepository.findByNameOrSerialNumber(text)
                .stream()
                .map(equipmentMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<String> findAllNames () {
        return equipmentRepository.findAll()
                .stream()
                .map(Equipment::getName)
                .collect(Collectors.toList());
    }

    public EquipmentDto save(EquipmentDto equipment) {
        return mapAndSaveEquipment(equipment);
    }

    public EquipmentDto update(EquipmentDto equipmentDto) {
        return mapAndSaveEquipment(equipmentDto);
    }

    private EquipmentDto mapAndSaveEquipment(EquipmentDto equipmentDto) {
        Equipment equipmentEntity = equipmentMapper.toEntity(equipmentDto);
        Equipment saveEquipment = equipmentRepository.save(equipmentEntity);
        return equipmentMapper.toDto(saveEquipment);
    }


}
