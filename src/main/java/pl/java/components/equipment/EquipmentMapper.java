package pl.java.components.equipment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java.components.category.Category;
import pl.java.components.category.CategoryRepository;

import java.util.Optional;

@Service
public class EquipmentMapper {

    private CategoryRepository categoryRepository;

    @Autowired
    public EquipmentMapper(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public EquipmentDto toDto(Equipment equipment) {
        EquipmentDto dto = new EquipmentDto();
        dto.setId(equipment.getId());
        dto.setName(equipment.getName());
        dto.setSerialNumber(equipment.getSerialNumber());
        if(equipment.getCategory() !=null) {
            dto.setCategory(equipment.getCategory().getName());
        }
        return dto;

    }

    public Equipment toEntity(EquipmentDto equipment) {
        Equipment entity = new Equipment();
        entity.setId(equipment.getId());
        entity.setName(equipment.getName());
        entity.setSerialNumber(equipment.getSerialNumber());
        Optional<Category> category = categoryRepository.findByName(equipment.getCategory());
        category.ifPresent(entity::setCategory);
        return entity;
    }
}
