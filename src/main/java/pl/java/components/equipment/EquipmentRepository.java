package pl.java.components.equipment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
    @Query("select e from Equipment e where lower(e.name) like lower(concat('%', :search, '%')) " +
            "or lower(e.serialNumber) like lower(concat('%', :search, '%'))")
    List<Equipment> findByNameOrSerialNumber(@Param("search") String search);

    Optional<Equipment> findBySerialNumber(String equipmentSerialNumber);
    Optional<Equipment> findByName(String equipmentName);
}
