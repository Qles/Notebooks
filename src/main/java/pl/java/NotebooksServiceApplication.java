package pl.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotebooksServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotebooksServiceApplication.class, args);
    }

}
