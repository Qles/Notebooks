package pl.java.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
        .jdbcAuthentication()
            .dataSource(dataSource);

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and()
         .authorizeRequests()
         .antMatchers("/index.html").permitAll()
        // .antMatchers("/api/repairs","/api/equipment","/api/clients").hasAnyRole("USER","ADMIN")
         //.antMatchers("/api/serviceman").hasRole("ADMIN")
                .anyRequest().authenticated()
         .and()
         .formLogin()
         .and()
         .logout()
         .and()
         .csrf().disable();
    }


}
