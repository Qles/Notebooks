angular.module('app')
    .config(function ($routeProvider) {
        $routeProvider
            .when('/servicemans', {
                templateUrl: 'app/components/servicemans/list/servicemansList.html',
                controller: 'ServicemansListController',
                controllerAs: 'ctrl'
            })
            .when('/serviceman-edit/:servicemanId', {
                templateUrl: 'app/components/servicemans/edit/servicemanEdit.html',
                controller: 'ServicemanEditController',
                controllerAs: 'ctrl'
            })
            .when('/serviceman-add', {
                templateUrl: 'app/components/servicemans/edit/servicemanEdit.html',
                controller: 'ServicemanEditController',
                controllerAs: 'ctrl'
            })
            .when('/serviceman-repairs/:servicemanId', {
                templateUrl: 'app/components/servicemans/repairs/servicemanRepairs.html',
                controller: 'ServicemanRepairsController',
                controllerAs: 'ctrl'
            })
            .when('/clients', {
                templateUrl: 'app/components/clients/list/clientsList.html',
                controller: 'ClientsListController',
                controllerAs: 'ctrl'
            })
            .when('/client-edit/:clientId', {
                templateUrl: 'app/components/clients/edit/clientEdit.html',
                controller: 'ClientEditController',
                controllerAs: 'ctrl'
            })
            .when('/client-add', {
                templateUrl: 'app/components/clients/edit/clientEdit.html',
                controller: 'ClientEditController',
                controllerAs: 'ctrl'
            })
            .when('/client-repairs/:clientId', {
                templateUrl: 'app/components/clients/repairs/clientRepairs.html',
                controller: 'ClientRepairsController',
                controllerAs: 'ctrl'
            })
            .when('/asset-history/:assetId', {
                templateUrl: 'app/components/assets/history/assetHistory.html',
                controller: 'AssetHistoryController',
                controllerAs: 'ctrl'
            })
            .when('/equipments', {
                templateUrl: 'app/components/equipments/list/equipmentsList.html',
                controller: 'EquipmentsListController',
                controllerAs: 'ctrl'
            })
            .when('/equipment-add', {
                templateUrl: 'app/components/equipments/edit/equipmentEdit.html',
                controller: 'EquipmentEditController',
                controllerAs: 'ctrl'
            })
            .when('/equipment-edit/:equipmentId', {
                templateUrl: 'app/components/equipments/edit/equipmentEdit.html',
                controller: 'EquipmentEditController',
                controllerAs: 'ctrl'
            })
            .when('/repairs', {
                templateUrl: 'app/components/repairs/list/repairsList.html',
                controller: 'RepairsListController',
                controllerAs: 'ctrl'
            })
            .when('/repair-edit/:repairId', {
                templateUrl: 'app/components/repairs/edit/repairEdit.html',
                controller: 'RepairEditController',
                controllerAs: 'ctrl'
            })
            .when('/repair-add', {
                templateUrl: 'app/components/repairs/edit/repairEdit.html',
                controller: 'RepairEditController',
                controllerAs: 'ctrl'
            })
            .otherwise({
                redirectTo: '/users'
            });
    });