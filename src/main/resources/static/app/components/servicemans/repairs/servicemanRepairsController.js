angular.module('app')
    .controller('ServicemanRepairsController', function($routeParams, ServicemansService) {
        const vm = this;
        const servicemanId = $routeParams.servicemanId;
        vm.serviceman = ServicemansService.get(servicemanId);
        vm.repairs = ServicemansService.getRepairs(servicemanId);

       // vm.finishAssignment = assignment => {
         //   AssignmentEndService.save(assignment.id)
             //   .then(response => {
             //       assignment.end = response.data;
            //    });
      //  };
    });