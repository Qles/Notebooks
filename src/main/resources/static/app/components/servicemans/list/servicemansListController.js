angular.module('app')
    .controller('ServicemansListController', function(ServicemansService) {
        const vm = this;
        vm.servicemans = ServicemansService.getAll();

        vm.search = initials => {
            vm.servicemans = ServicemansService.getAll({initials});
        };
    });