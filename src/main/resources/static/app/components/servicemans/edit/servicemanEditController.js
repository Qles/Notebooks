angular.module('app')
    .controller('ServicemanEditController', function($routeParams, $location, $timeout, ServicemansService, Serviceman) {
        const vm = this;
        const servicemanId = $routeParams.servicemanId;
        if(servicemanId)
            vm.serviceman = ServicemansService.get(servicemanId);
        else
            vm.serviceman = new Serviceman();

        const saveCallback = () => {
            $location.path(`/serviceman-edit/${vm.serviceman.id}`);
        };
        const errorCallback = err => {
            vm.msg=`Błąd zapisu: ${err.data.message}`;
        };

        vm.saveServiceman = () => {
            ServicemansService.save(vm.serviceman)
                .then(saveCallback)
                .catch(errorCallback);
        };

        const updateCallback = response => vm.msg='Zapisano zmiany';
        vm.updateServiceman = () => {
            ServicemansService.update(vm.serviceman)
                .then(updateCallback)
                .catch(errorCallback);
        };

    });