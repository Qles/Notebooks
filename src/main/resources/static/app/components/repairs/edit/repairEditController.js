angular.module('app')
    .controller('RepairEditController', function($routeParams, $location, $timeout, RepairsService, Repair, ClientsService, EquipmentsService, ServicemansService, StatusService) {
        const vm = this;
        const repairId = $routeParams.repairId;
        if(repairId)
            vm.repair = RepairsService.get(repairId);

        else
            vm.repair = new Repair();
            vm.clientNames = ClientsService.getAllNames();
            vm.equipmentNames = EquipmentsService.getAllNames();
            vm.servicemanInitials = ServicemansService.getAllInitials();
            vm.status = StatusService.getAllNames();

        const saveCallback = () => {
            $location.path(`/repair-edit/${vm.repair.id}`);
        };
        const errorCallback = err => {
            vm.msg=`Błąd zapisu: ${err.data.message}`;
        };

        vm.saveRepair = () => {
            RepairsService.save(vm.repair)
                .then(saveCallback)
                .catch(errorCallback);
        };

        const updateCallback = response => vm.msg='Zapisano zmiany';
        vm.updateRepair = () => {
            RepairsService.update(vm.repair)
                .then(updateCallback)
                .catch(errorCallback);
        };

    });