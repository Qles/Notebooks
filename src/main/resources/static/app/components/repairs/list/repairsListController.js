angular.module('app')
    .controller('RepairsListController', function(RepairsService) {
        const vm = this;
        vm.repairs = RepairsService.getAll();

        vm.search = text => {
            vm.repairs = RepairsService.getAll({text});
        };
    });