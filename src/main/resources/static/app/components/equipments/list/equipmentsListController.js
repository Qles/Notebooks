angular.module('app')
    .controller('EquipmentsListController', function(EquipmentsService) {
        const vm = this;
        vm.equipments = EquipmentsService.getAll();

        vm.search = text => {
            vm.equipments = EquipmentsService.getAll({text});
        };
    });