angular.module('app')
    .controller('EquipmentEditController', function($routeParams, $location, $timeout, EquipmentsService, Equipment, CategoryService) {
        const vm = this;
        const equipmentId = $routeParams.equipmentId;
        if(equipmentId)
            vm.equipment = EquipmentsService.get(equipmentId);
        else
            vm.equipment = new Equipment();
            vm.categoryNames = CategoryService.getAllNames();


        const saveCallback = () => {
            $location.path(`/equipment-edit/${vm.equipment.id}`);
        };
        const errorCallback = err => {
            vm.msg=`Błąd zapisu: ${err.data.message}`;
        };

        vm.saveEquipment = () => {
            EquipmentsService.save(vm.equipment)
                .then(saveCallback)
                .catch(errorCallback);
        };

        const updateCallback = response => vm.msg='Zapisano zmiany';
        vm.updateEquipment = () => {
            EquipmentsService.update(vm.equipment)
                .then(updateCallback)
                .catch(errorCallback);
        };

    });