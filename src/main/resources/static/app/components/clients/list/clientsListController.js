angular.module('app')
    .controller('ClientsListController', function(ClientsService) {
        const vm = this;
        vm.clients = ClientsService.getAll();

        vm.search = text => {
            vm.clients = ClientsService.getAll({text});
        };
    });