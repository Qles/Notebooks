angular.module('app')
    .controller('ClientEditController', function($routeParams, $location, $timeout, ClientsService, Client) {
        const vm = this;
        const clientId = $routeParams.clientId;
        if(clientId)
            vm.client = ClientsService.get(clientId);

        else
            vm.client = new Client();


        const saveCallback = () => {
            $location.path(`/client-edit/${vm.client.id}`);
        };
        const errorCallback = err => {
            vm.msg=`Błąd zapisu: ${err.data.message}`;
        };

        vm.saveClient = () => {
            ClientsService.save(vm.client)
                .then(saveCallback)
                .catch(errorCallback);
        };

        const updateCallback = response => vm.msg='Zapisano zmiany';
        vm.updateClient = () => {
            ClientsService.update(vm.client)
                .then(updateCallback)
                .catch(errorCallback);
        };

    });