angular.module('app')
    .controller('ClientRepairsController', function($routeParams, ClientsService) {
        const vm = this;
        const clientId = $routeParams.clientId;
        vm.client = ClientsService.get(clientId);
        vm.repairs = ClientsService.getRepairs(clientId);

       // vm.finishAssignment = assignment => {
         //   AssignmentEndService.save(assignment.id)
             //   .then(response => {
             //       assignment.end = response.data;
            //    });
      //  };
    });