angular.module('app')
    .constant('EQUIPMENT_ENDPOINT', '/api/equipments/:id')
    .constant('EQUIPMENT_NAMES_ENDPOINT', '/api/equipments/names')
    .constant('EQUIPMENT_REPAIRS_ENDPOINT', '/api/users/:id/assignments')
    .factory('Equipment', function($resource, EQUIPMENT_ENDPOINT, CLIENT_REPAIRS_ENDPOINT) {
        return $resource(EQUIPMENT_ENDPOINT, { id: '@_id' }, {
            update: {
                method: 'PUT'
            },
            getAssignments: {
                method: 'GET',
                url: CLIENT_REPAIRS_ENDPOINT,
                params: {id: '@id'},
                isArray: true
            }
        });
    })
    .service('EquipmentsService', function($resource, EQUIPMENT_NAMES_ENDPOINT, Equipment) {
        this.getAll = params => Equipment.query(params);
        this.get = index => Equipment.get({id: index});
        this.getAssignments = index => Equipment.getAssignments({id: index});
        this.save = equipment => equipment.$save();
        this.update = equipment => equipment.$update({id: equipment.id})
        this.getAllNames = () => $resource(EQUIPMENT_NAMES_ENDPOINT).query();
    });