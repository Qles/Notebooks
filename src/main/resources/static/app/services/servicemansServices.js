angular.module('app')
    .constant('SERVICEMAN_ENDPOINT', '/api/servicemans/:id')
    .constant('SERVICEMAN_INITIALS_ENDPOINT', '/api/servicemans/initials')
    .constant('SERVICEMAN_REPAIRS_ENDPOINT', '/api/servicemans/:initials/repairs')
    .factory('Serviceman', function($resource, SERVICEMAN_ENDPOINT, SERVICEMAN_REPAIRS_ENDPOINT) {
        return $resource(SERVICEMAN_ENDPOINT, { id: '@_id' }, {
            update: {
                method: 'PUT'
            },
            getRepairs: {
                method: 'GET',
                url: SERVICEMAN_REPAIRS_ENDPOINT,
                params: {id: '@id'},
                isArray: true
            }
        });
    })
    .service('ServicemansService', function($resource, SERVICEMAN_INITIALS_ENDPOINT, Serviceman) {
        this.getAll = params => Serviceman.query(params);
        this.get = index => Serviceman.get({id: index});
        this.getRepairs = index => Serviceman.getRepairs({initials: index});
        this.save = serviceman => serviceman.$save();
        this.update = serviceman => serviceman.$update({id: serviceman.id})
        this.getAllInitials = () => $resource(SERVICEMAN_INITIALS_ENDPOINT).query();
    });