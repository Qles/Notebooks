angular.module('app')
    .constant('CLIENT_ENDPOINT', '/api/clients/:id')
    .constant('CLIENT_NAMES_ENDPOINT', '/api/clients/names')
    .constant('CLIENT_REPAIRS_ENDPOINT', '/api/clients/:names/repairs')
    .factory('Client', function($resource, CLIENT_ENDPOINT, CLIENT_REPAIRS_ENDPOINT) {
        return $resource(CLIENT_ENDPOINT, { id: '@_id' }, {
            update: {
                method: 'PUT'
            },
            getRepairs: {
                method: 'GET',
                url: CLIENT_REPAIRS_ENDPOINT,
                params: {id: '@id'},
                isArray: true
            }
        });
    })
    .service('ClientsService', function($resource, CLIENT_NAMES_ENDPOINT, Client) {
        this.getAll = params => Client.query(params);
        this.get = index => Client.get({id: index});
        this.getRepairs = index => Client.getRepairs({names: index});
        this.save = client => client.$save();
        this.update = client => client.$update({id: client.id})
        this.getAllNames = () => $resource(CLIENT_NAMES_ENDPOINT).query();
    });