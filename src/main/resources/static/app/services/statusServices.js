angular.module('app')
    .constant('STATUS_NAMES_ENDPOINT', '/api/statuses/names')
    .service('StatusService', function($resource, STATUS_NAMES_ENDPOINT) {
        this.getAllNames = () => $resource(STATUS_NAMES_ENDPOINT).query();
    });