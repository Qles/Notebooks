angular.module('app')
    .constant('REPAIR_ENDPOINT', '/api/repairs/:id')
    .constant('REPAIR_REPAIRS_ENDPOINT', '/api/servicemans/:id/repairs')
    .factory('Repair', function($resource, REPAIR_ENDPOINT, REPAIR_REPAIRS_ENDPOINT) {
        return $resource(REPAIR_ENDPOINT, { id: '@_id' }, {
            update: {
                method: 'PUT'
            },
            getRepairs: {
                method: 'GET',
                url: REPAIR_REPAIRS_ENDPOINT,
                params: {id: '@id'},
                isArray: true
            }
        });
    })
    .service('RepairsService', function(Repair) {
        this.getAll = params => Repair.query(params);
        this.get = index => Repair.get({id: index});
        this.getRepairs = index => Repair.getRepais({id: index});
        this.save = repair => repair.$save();
        this.update = repair => repair.$update({id: repair.id})
    });