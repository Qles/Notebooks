

insert into serviceman(first_name, last_name, initials) values ("Przemysław", "Giża", "PG");
insert into serviceman(first_name, last_name, initials) values ("Dariusz" , "Drwal" , "DG");

insert into client(name, address, postal_code, city, telephone_number) values ("Kamila Król-Giża", "Zagłoby 5", "33-100", "Tarnów", "504-338-174");
insert into client(name, address, postal_code, city, telephone_number) values ("Wiktorek Giża", "Jodłówka-Wałki 76E", "33-150", "Jodłówka-Wałki", "507-029-822");
insert into client(name, address, postal_code, city, telephone_number, nip) values ("Notebooks-Service Dariusz Drwal", "Boya Żeleńśkiego 4", "33-100", "Tarnów", "507-029-822",9930353571);

insert into category(name, description) values ("Laptop", "Przenośnie urządzenia");
insert into category(name, description) values ("Tablet", "Urządzenia mobilne");

insert into status(name) values ("Przyjęty do serwisu");
insert into status(name) values ("Czeka na zgodę klienta");
insert into status(name) values ("Zgoda na naprawę");
insert into status(name) values ("Naprawiony - czeka na odbiór");
insert into status(name) values ("Zwrot - czeka na odbiór");

insert into equipment(name, serial_number, category_id) values ("Dell Inspiron 15-5437", "1234566", 1);
insert into equipment(name, serial_number, category_id) values ("Lenovo Thinkpad L520", "L9/241/424", 2);

insert into repair(start, end, warranty, price, cost, status_id, defect, repair_description, client_id, equipment_id, serviceman_id) values ('2019-07-11', '2019-07-12', '2020-02-12', 250, 100, 1, "zwarcie", "Zwarcie w obwodzie zasilania procesora", 1,1,1);
insert into repair(start, end, status_id, defect, repair_description, client_id, equipment_id, serviceman_id) values ('2019-07-11', '2019-07-12', 2, "zalany", "Wymiana płyty głównej na nową ", 2,2,2);

